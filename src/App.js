import React, { Component } from 'react';
import Projects from './Component/Projects'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
         <h4>To Dos</h4>
        </p>
        
        <Projects/>
      </div> 
      
    );
  }
}

export default App;
